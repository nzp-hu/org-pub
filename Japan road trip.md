# Térkép
https://www.google.com/maps/d/edit?mid=1Nt09H0tlQGlopk4xAzYlwHl-DPXoakKq&usp=sharing

---

# Osaka
Laidback & chill ppl, **food heaven**

Minami region is close to most of the attractions

## Random destinations
- **zaba**
- **takoyaki**
- Dotonbori area (main attraction)
- Ichiran ramen (food chain) #zaba 
- Osaka castle
- Amerikamura - fashion street?
- Umeda sky building
- Okaru - okonomiyaki restaurant #zaba 
- Osaka aquarium kaiyukan
- Shinsekai
- Shinsaibashi (shopping district)
- Kukuru takoyaki (restaurant) #zaba

---

# Koyasan
Heritage; 100+ temples

- Saizen-in #accomodation
- Danjo garan temple complex
- Daimon gate
- Kongobu-ji (head temple) + Banryutei (largest rock garden)
- **Okunoin cemetery**

---

# Nara
Deers, parks, shrines

## Random destinations
- **Nara park**
- Todai-ji (temple)
- Kasuga-taisha
- Mount wakakusa
- **Mochi pounding "show"** @ Nakatanidou #zaba 
- Isui-en garden
- Nara national museum
- Naramichi (old merchant district)
- Lamp bar

---

# Kyoto
- Vibrant nightlife
- Most tourist acitivities in Gion area

## Pontocho #zaba
Atmospheric dining street

## Random destinations
- Arashiyama, **Arashiyama bamboo grove** --> alternatíva: Kamakurában kis "bambuszerdő", ott kevesebben vannak
- **Tenryu-ji**
- Senko-ji (temple)
- Iwatayama monkey park
- Kinkaku-ji (temple)
- Ramen sen no kaze (ramen shop)
- Nishiki market #zaba
- Gion (entertainment district)
- **Fushimi inari** (shrine)
- Nanzen-ji (temple)
- Eikan-do (temple)
- Pontocho
- **Kiyomizu-dera** (temple)
- Nijo castle
- Kibune & Kurama
- Kagizen Yoshifuza - traditional Japanese sweets #zaba
- Yasaka shrine
- **Sagano scenic railway**
- Kyo-kaiseki dinner #zaba  ([restaurant recommendations](https://youtu.be/F0AT_7uVbeo?t=688))

---

# Kanazawa - "little Kyoto"
- temples, gardens
- Omicho market
- DT Suzuki museum
- 21st century museum of contemporary art
- Oyama shrine
- Kanazawa castle
- Kenroku-en garden
- Higashi chaya district
- Menya taiga - black miso ramen
- Tsuzumi gate

---

# Takayama
Beef, sake, onsens.

- Takayama jinya
- Sanmachi suji: sake breweries
-  Hida folk village
- **Yukimurasaki** #accomodation - private onsen, rooftop onsen
-  Shinhotaka ropeway

---

# Tokyo
**Shinjuku district:** nightlife, drinking scene

## Akihabara
- Don Quijite shop: souveniers
- Arcades?

## Random destinations
- Zen Tokyo #accomodation - capsule hotel
- Meiji shrine
- Harajuku, Takashi district
- **Shibuya crossing**
- **Akihabara**
- Teamlab borderless - digital museum
- **Omoide yokocho** ("Memory lane")
- Tokyo tower
- Senso-ji temple
- Kabukicho (entertainment district in Shinjuku)
- Tokyo skytree
- Toyosu fish market
- Nezu shrine
- Shinjuku gyoen shrine

---

# Food notes
## (good) Food chains
- Ichiran (ramen)
- Afuri (yusu-flavoured ramens)
- Nakiryo
- Uobei sushi, Genki sushi (conveyor-belt style)
- Manten sushi ("premium" sushi?)

## Cheaper options
- Kyubey
- Sushi no Midori
- Zanmai sushi
- Konbini food

---

# Generic notes
## Random notes
- bento boxes at train stations
- **IC card:** prepaid travel card + can be used in convenience stores, shops, restaurants, cafes, vending machines etc.
- Luggage delivery services (https://youtu.be/00ZXaXIABMY?t=51)
- SIM w/ internet access can be purchased @ the airport (possibility to pre-book/preorder SIM+device and pick it up upon arrival) **OR** https://www.simcardgeek.com/
	- Unlimited data, 21 days, # ¥6,980: https://www.simcardgeek.com/product/japan-data-sim-card-unlimited-data-21-days/
	- https://www.mobal.com/japan-sim-card/?source=3146#buysim
- Rail pass?
- Mt. Fuji best view from seat E (seat D on green class) on the Shinkansen (https://youtu.be/00ZXaXIABMY?t=485)
- Table manners: https://www.japan-guide.com/e/e2005.html
- Konbiniben (FamilyMart) a hűtőpultban(?) kapható pudingos taiyaki

## How to pay respects at a shrine
1. Ring the bell (if there is one)
2. Bow twice
3. Clap twice
4. Bow once

## 13 Places to See The Best View of Mt. Fuji
https://blog.japanwondertravel.com/best-viewing-spots-mt-fuji-12238

---

# Sources
## Allan Su: How to Spend 14 Days in Japan - A Japan Travel Itinerary
https://www.youtube.com/watch?v=IuTDuvYr7f0

Budget breakdown:

|                   |     cost |
| ----------------- | --------:|
| Transportation    |  500 USD |
| Accomodation      | 1200 USD |
| Food + activities | 1400 USD |
|                   |          |
| Total for 2 ppl   | 3100 USD |

## japan-guide.com: 15 Tips for First-Time Travellers to Japan
https://www.youtube.com/watch?v=00ZXaXIABMY